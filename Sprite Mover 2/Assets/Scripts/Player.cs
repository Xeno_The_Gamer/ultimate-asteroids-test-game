﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public float maxspeed = 4;
	public float rotspeed = 3;
	public bool isEnabled = true;
	public Transform leftBlaster;
	public Transform rightBlaster;
	public GameObject bulletPrefab;
	public float bulletForce = 2;
	public float fireRate = 0.2f;
	private float fireTimer = 0;

	// Update is called once per frame
	void Update ()
	{
		if (isEnabled) {
			// WASD Controls
			if (Input.GetKey (KeyCode.W)) {
				transform.Translate (Vector2.up * maxspeed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.S)) {
				transform.Translate (Vector2.down * maxspeed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.A)) {
				transform.Rotate (Vector3.forward * rotspeed);
			}
			if (Input.GetKey (KeyCode.D)) {
				transform.Rotate (Vector3.back * rotspeed);
			}
			// Arrow Key Controls
			if (Input.GetKey (KeyCode.UpArrow)) {
				transform.Translate (Vector2.up * maxspeed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.DownArrow)) {
				transform.Translate (Vector2.down * maxspeed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.LeftArrow)) {
				transform.Rotate (Vector3.forward * rotspeed);
			}
			if (Input.GetKey (KeyCode.RightArrow)) {
				transform.Rotate (Vector3.back * rotspeed);
			}

			if (Input.GetKey (KeyCode.Space)) {
				fireTimer += Time.deltaTime;
				if (fireTimer >= fireRate) {
					fireTimer = 0;


					//fires the guns
					GameObject bulletL = Instantiate (bulletPrefab, leftBlaster.transform.position, leftBlaster.transform.rotation);
					bulletL.GetComponent <Rigidbody2D> ().AddForce (bulletL.transform.up * bulletForce, ForceMode2D.Impulse);
					GameObject bulletR = Instantiate (bulletPrefab, rightBlaster.transform.position, rightBlaster.transform.rotation);
					bulletR.GetComponent <Rigidbody2D> ().AddForce (bulletR.transform.up * bulletForce, ForceMode2D.Impulse);
				}
			}
			if (Input.GetKeyUp (KeyCode.Space)) {
				fireTimer = fireRate;
			}
		}
	}
}