﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Movement : MonoBehaviour {
	public float maxspeed = 4;
	public float rotspeed = 3;
	public bool isEnabled = true;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isEnabled) {
			// WASD Controls
			if (Input.GetKey (KeyCode.W)) {
				transform.Translate (Vector2.up * maxspeed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.S)) {
				transform.Translate (Vector2.down * maxspeed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.A)) {
				transform.Translate (Vector2.left * rotspeed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.D)) {
				transform.Translate (Vector2.right * rotspeed * Time.deltaTime);
			}
			// Arrow Key Controls
			if (Input.GetKey (KeyCode.UpArrow)) {
				transform.Translate (Vector2.up * maxspeed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.DownArrow)) {
				transform.Translate (Vector2.down * maxspeed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.LeftArrow)) {
				transform.Translate (Vector2.left * rotspeed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.RightArrow)) {
				transform.Translate (Vector2.right * rotspeed * Time.deltaTime);
			}
		}
	}
}
